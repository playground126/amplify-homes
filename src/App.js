import './App.css';
import ActionCard from './ui-components/ActionCard';

function App() {
  return (
    <div className="App">
      <ActionCard />
    </div>
  );
}

export default App;
